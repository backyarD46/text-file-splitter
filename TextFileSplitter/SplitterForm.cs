﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextFileSplitter
{
    public partial class splitterForm : Form
    {
        /// <summary>
        /// ファイル分割時に分割条件の単語をどこに付けるかの指定用。
        /// </summary>
        private enum SplitWordOption
        {
            // 削除する（出力に含めない）
            Erase,
            // 出力ファイルの末尾に付ける
            Tail,
            // 出力ファイルの先頭に付ける
            Head,
        }

        public splitterForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ファイル選択ボタン押下時処理。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileSelectButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileSelectDlg = new OpenFileDialog();
            fileSelectDlg.Multiselect = false;
            DialogResult result = fileSelectDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                fileNameText.Text = fileSelectDlg.FileName;
            }
        }

        /// <summary>
        /// 閉じるボタン押下時処理。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 処理実行ボタン押下時処理。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void executeButton_Click(object sender, EventArgs e)
        {
            if (fileNameText.Text.Length > 0)
            {
                // 分割条件単語の付け方に関する指定からoption変数を設定する。
                SplitWordOption option=SplitWordOption.Erase;
                if (splitWordExclude.Checked) option = SplitWordOption.Erase;
                if (splitWordTail.Checked) option = SplitWordOption.Tail; else option = SplitWordOption.Head;
                // 分割実施。
                ExecuteSplit(fileNameText.Text, splitWordText.Text, option);
            }
            else
            {
                MessageBox.Show("ファイルを指定してください。", "警告", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        /// <summary>
        /// ファイル分割処理本体。
        /// </summary>
        /// <param name="fileName">Target file for split.</param>
        private void ExecuteSplit(string fileName, string splitWord, SplitWordOption option)
        {
            int fileIndex = 1;
            StreamWriter writer = new StreamWriter(string.Format("output_{0:D3}.txt",fileIndex));

            using (StreamReader reader = new StreamReader(fileName))
            {
                progressText.Text += "■ 処理開始";
                progressText.Text += ("対象ファイル: " + fileNameText.Text + "\r\n");
                progressText.Text += ("分割条件単語: " + splitWord + "\r\n");

                // 行データ読み込み用変数
                string lineData = string.Empty;
                // 分割文字
                string[] splitWords = { splitWord };

                while (!reader.EndOfStream)
                {
                    lineData = reader.ReadLine();
                    string[] work = lineData.Split(splitWords, StringSplitOptions.None);
                    if (work.Length>1)
                    {
                        // 分割文字が行に含まれていた場合
                        progressText.Text += (work[0] + "\r\n" + "***" + splitWord + "***" + "\r\n" + work[1] + "\r\n");
                        // 前半をファイルに書き込む
                        if (option == SplitWordOption.Tail)
                        {
                            writer.WriteLine(work[0] + splitWord);
                        }
                        else
                        {
                            writer.WriteLine(work[0]);
                        }
                        // 新しいファイルを用意する。
                        writer.Close();
                        writer = new StreamWriter(string.Format("output_{0:D3}.txt", ++fileIndex));
                        if (option == SplitWordOption.Head)
                        {
                            writer.WriteLine(splitWord + work[1]);
                        }
                        else
                        {
                            writer.WriteLine(work[1]);
                        }
                    }
                    else
                    {
                        // 分割文字が行に含まれない場合 …… そのまま書き込む。
                        progressText.Text += (lineData + "\r\n");
                        writer.WriteLine(lineData);
                    }
                }
            }
            writer.Close();
            writer.Dispose();
        }
    }
}
