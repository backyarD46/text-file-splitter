﻿namespace TextFileSplitter
{
    partial class splitterForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.topPanel = new System.Windows.Forms.Panel();
            this.executeButton = new System.Windows.Forms.Button();
            this.fileSelectButton = new System.Windows.Forms.Button();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.fileNameText = new System.Windows.Forms.TextBox();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.closeButton = new System.Windows.Forms.Button();
            this.mailPanel = new System.Windows.Forms.Panel();
            this.splitWordText = new System.Windows.Forms.TextBox();
            this.splitWordLabel = new System.Windows.Forms.Label();
            this.progressText = new System.Windows.Forms.TextBox();
            this.splitWordExclude = new System.Windows.Forms.RadioButton();
            this.splitWordTail = new System.Windows.Forms.RadioButton();
            this.splitWordHead = new System.Windows.Forms.RadioButton();
            this.topPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.mailPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.splitWordHead);
            this.topPanel.Controls.Add(this.splitWordTail);
            this.topPanel.Controls.Add(this.splitWordExclude);
            this.topPanel.Controls.Add(this.splitWordLabel);
            this.topPanel.Controls.Add(this.splitWordText);
            this.topPanel.Controls.Add(this.executeButton);
            this.topPanel.Controls.Add(this.fileSelectButton);
            this.topPanel.Controls.Add(this.fileNameLabel);
            this.topPanel.Controls.Add(this.fileNameText);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1067, 94);
            this.topPanel.TabIndex = 0;
            // 
            // executeButton
            // 
            this.executeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.executeButton.Location = new System.Drawing.Point(935, 58);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(120, 30);
            this.executeButton.TabIndex = 3;
            this.executeButton.Text = "処理開始";
            this.executeButton.UseVisualStyleBackColor = true;
            this.executeButton.Click += new System.EventHandler(this.executeButton_Click);
            // 
            // fileSelectButton
            // 
            this.fileSelectButton.Location = new System.Drawing.Point(781, 8);
            this.fileSelectButton.Name = "fileSelectButton";
            this.fileSelectButton.Size = new System.Drawing.Size(37, 25);
            this.fileSelectButton.TabIndex = 2;
            this.fileSelectButton.Text = "…";
            this.fileSelectButton.UseVisualStyleBackColor = true;
            this.fileSelectButton.Click += new System.EventHandler(this.fileSelectButton_Click);
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.AutoSize = true;
            this.fileNameLabel.Location = new System.Drawing.Point(12, 12);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(107, 17);
            this.fileNameLabel.TabIndex = 1;
            this.fileNameLabel.Text = "読み込むファイル名";
            // 
            // fileNameText
            // 
            this.fileNameText.Location = new System.Drawing.Point(125, 9);
            this.fileNameText.Name = "fileNameText";
            this.fileNameText.Size = new System.Drawing.Size(650, 24);
            this.fileNameText.TabIndex = 0;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.closeButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 554);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(1067, 59);
            this.bottomPanel.TabIndex = 1;
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.Location = new System.Drawing.Point(932, 17);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(120, 30);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = "閉じる";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // mailPanel
            // 
            this.mailPanel.Controls.Add(this.progressText);
            this.mailPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mailPanel.Location = new System.Drawing.Point(0, 94);
            this.mailPanel.Name = "mailPanel";
            this.mailPanel.Size = new System.Drawing.Size(1067, 460);
            this.mailPanel.TabIndex = 2;
            // 
            // splitWordText
            // 
            this.splitWordText.Location = new System.Drawing.Point(125, 39);
            this.splitWordText.Name = "splitWordText";
            this.splitWordText.Size = new System.Drawing.Size(161, 24);
            this.splitWordText.TabIndex = 4;
            // 
            // splitWordLabel
            // 
            this.splitWordLabel.AutoSize = true;
            this.splitWordLabel.Location = new System.Drawing.Point(12, 42);
            this.splitWordLabel.Name = "splitWordLabel";
            this.splitWordLabel.Size = new System.Drawing.Size(86, 17);
            this.splitWordLabel.TabIndex = 5;
            this.splitWordLabel.Text = "分割条件単語";
            // 
            // progressText
            // 
            this.progressText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressText.Location = new System.Drawing.Point(0, 0);
            this.progressText.Multiline = true;
            this.progressText.Name = "progressText";
            this.progressText.Size = new System.Drawing.Size(1067, 460);
            this.progressText.TabIndex = 0;
            // 
            // splitWordExclude
            // 
            this.splitWordExclude.AutoSize = true;
            this.splitWordExclude.Checked = true;
            this.splitWordExclude.Location = new System.Drawing.Point(292, 40);
            this.splitWordExclude.Name = "splitWordExclude";
            this.splitWordExclude.Size = new System.Drawing.Size(138, 21);
            this.splitWordExclude.TabIndex = 6;
            this.splitWordExclude.TabStop = true;
            this.splitWordExclude.Text = "分割条件単語を消す";
            this.splitWordExclude.UseVisualStyleBackColor = true;
            // 
            // splitWordTail
            // 
            this.splitWordTail.AutoSize = true;
            this.splitWordTail.Location = new System.Drawing.Point(436, 40);
            this.splitWordTail.Name = "splitWordTail";
            this.splitWordTail.Size = new System.Drawing.Size(181, 21);
            this.splitWordTail.TabIndex = 7;
            this.splitWordTail.Text = "分割条件単語を末尾につける";
            this.splitWordTail.UseVisualStyleBackColor = true;
            // 
            // splitWordHead
            // 
            this.splitWordHead.AutoSize = true;
            this.splitWordHead.Location = new System.Drawing.Point(623, 40);
            this.splitWordHead.Name = "splitWordHead";
            this.splitWordHead.Size = new System.Drawing.Size(181, 21);
            this.splitWordHead.TabIndex = 8;
            this.splitWordHead.Text = "分割条件単語を冒頭につける";
            this.splitWordHead.UseVisualStyleBackColor = true;
            // 
            // splitterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 613);
            this.Controls.Add(this.mailPanel);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.topPanel);
            this.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "splitterForm";
            this.Text = "Text File Splitter";
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.mailPanel.ResumeLayout(false);
            this.mailPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Button fileSelectButton;
        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.TextBox fileNameText;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Panel mailPanel;
        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.Label splitWordLabel;
        private System.Windows.Forms.TextBox splitWordText;
        private System.Windows.Forms.TextBox progressText;
        private System.Windows.Forms.RadioButton splitWordHead;
        private System.Windows.Forms.RadioButton splitWordTail;
        private System.Windows.Forms.RadioButton splitWordExclude;
    }
}

